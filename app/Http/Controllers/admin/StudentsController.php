<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\DepartmentClass;
use App\Faculty;
use App\Helpers\UUID;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\RegisteredStudent;
use App\Student;
use App\StudentEmail;
use App\User;
use Excel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class StudentsController extends Controller
{
//    bcrypt
    public function index()
    {
        $faculites = Faculty::all();
//        $students=Student::all();
        return view('admin.students.index', compact('faculites'));
    }
	
	public function studentsReset()
	{
		$allstudents=Student::all();
		foreach($allstudents as $key=>$student)
		{
			$student->STUDENT_PASSWORD=bcrypt($student->STUDENT_SSN);
			if($student->user)
			{
				
				$student->user->email=$student->STUDENT_SSN;
				$student->user->password=bcrypt($student->STUDENT_SSN);
				$student->user->save();
			}
			$student->save();
		}
		
		
		return 'done';
	}
	public function randomPassword()
	{
		$token = substr(Password::getRepository()->createNewToken(), 0, 8);
		return $token.'\r\n'.bcrypt($token);
	}
    public function registerStudent()
    {
        $faculites = Faculty::all();
//        $students=Student::all();
        return view('student.register', compact('faculites'));
    }
    public function registerStudentArabic()
    {
        $faculites = Faculty::all();
//        $students=Student::all();
        return view('student.registerarabic', compact('faculites'));
    }
    public function studentsRegisterationCounter()
    {
        $faculites = Faculty::all();
//        $students=Student::all();
        return view('student.studentsRegisterationCounter', compact('faculites'));
    }
    public function enqueryMail()
    {
        return view('student.enquerymail');
    }
    public function studentsMails()
    {
        return view('student.studentsmails');
    }
    public function complains()
    {
        return view('student.complains');
    }
    public function faq()
    {
        return view('student.faq');
    }
    public function registerationHome()
    {
        return view('student.registerationHome');
    }
    public function getStudentsRegisterationCount(Request $request)
    {

        if ($request->id == 0) {
            return RegisteredStudent::all()->count();
        } else {
            // code...

            return RegisteredStudent::where('FACULTY_ID', $request->id)->get()->count();
        }
    }
    public function getStudentsRegisterationCountTable(Request $request)
    {

        $str = '<table class="table table-striped"><thead><tr><th>رقم</th><th>الكلية</th><th>العدد</th></tr></thead><tbody>';

        if ($request->id == 0) {
            $allfaculties = Faculty::all();

            foreach ($allfaculties as $key => $faculty) {

                $count = RegisteredStudent::where('FACULTY_ID', $faculty->id)->get()->count();

                $str .= '<tr><td>' . $key . '</td><td>' . $faculty->FACULTY_NAME . '</td><td>' . $count . '</td></tr>';

            }

            //return RegisteredStudent::all()->count();
        } else {
            // code...
            $count = RegisteredStudent::where('FACULTY_ID', $request->id)->get()->count();
            $faculty = Faculty::where('id', $request->id)->get()->first();
            $str .= '<tr><td>1</td><td>' . $faculty->FACULTY_NAME . '</td><td>' . $count . '</td></tr>';

            //return RegisteredStudent::where('FACULTY_ID',$request->id)->get()->count();
        }
        $str .= '</tbody></table>';
        return $str;
    }
    public function deleteStudentMail(Request $request)
    {
        $n = StudentEmail::where('student_ssn', $request->ssn)->delete();
        if ($n > 0) {
            return 'yes';
        } else {
            return 'no';
        }
    }
	 public function deleteStudentMailByMail(Request $request)
    {
		//return $request->email;
      $n=StudentEmail::where('student_email',$request->email)->delete();
      if($n>0)
      return 'yes';
      else {
        return 'no';
      }
    }
    public function storeStudentMail(Request $request)
    {
        $existstudent = StudentEmail::where('student_ssn', $request->ssn)->get()->first();
        $existmail = StudentEmail::where('student_email', $request->email)->get()->first();

        $validdomain1 = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)*(aun.edu.eg)$/ix", $request->email);
        $validdomain2 = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+(au.edu.eg)$/ix", $request->email);
        if (!$validdomain1 && !$validdomain2) {
            return 'ليس بريد اكاديمى';
        }
        if (($existstudent != null && $existmail != null && $existstudent->student_ssn != $existmail->student_ssn) || ($existstudent == null && $existmail != null)) {
            return 'البريد الاكاديمى مستخدم بالفعل';
        }

        if ($existstudent) {
            $n = \DB::table('student_emails')
                ->where('student_ssn', $existstudent->student_ssn)
                ->update(['student_name' => $request->name, 'student_email' => $request->email, 'student_password' => $request->password]);
            // $existstudent->student_name=$request->name;
            //
            // $existstudent->student_email=$request->email;
            //
            // $existstudent->student_password=$request->password;
            //
            // $existstudent->save();
            // return '1';
        } else {
            $newstudentmail = new StudentEmail();
            $newstudentmail->student_ssn = $request->ssn;
            $newstudentmail->student_name = $request->name;
            $newstudentmail->student_email = $request->email;
            $newstudentmail->student_password = $request->password;
            $newstudentmail->save();
        }
        return 'yes';
    }
    public function existEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            //return back()->withErrors(['error' => 'Email Found Before']);
            return 'yes';
        }

        return 'no';
    }
    public function storeStudent(Request $request, $english = '1', $newRegisteration = '1')
    {
        $faculites = Faculty::all();
        $student = Student::where('STUDENT_SSN', $request->ssn)->first();
        $user = User::where('email', $request->email)->first();

        if (($user != null && $newRegisteration == '1') || ($user != null && $newRegisteration == '0' && $student->user_id != $user->id)) {
            //dd($student->user_id.' - '. $user->id.' - '.$newRegisteration);
            //return back()->withErrors(['error' => 'Email Found Before']);
            //dd('1');
            $responsecode = 2;
            if ($english == '1') {
                return view('student.register', compact('faculites', 'responsecode'));
            } else {
                return view('student.registerarabic', compact('faculites', 'responsecode'));

            }

        }
        //dd('2');
        $validdomain1 = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)*(aun.edu.eg)$/ix", $request->email);
        $validdomain2 = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+(au.edu.eg)$/ix", $request->email);
        if (!$validdomain1 && !$validdomain2) {
            $responsecode = 3;
            if ($english == 1) {
                return view('student.register', compact('faculites', 'responsecode'));
            } else {
                return view('student.registerarabic', compact('faculites', 'responsecode'));

            }

        }

//        dd($request->all());
        if (!$student && $newRegisteration == '1') {
            $user = new User();
            $student = new Student();
            $studentinformation = StudentEmail::where('student_ssn', $request->ssn)->get()->first();

            if ($studentinformation) {
                $student->in_mis = 1;
            } else {
                $student->in_mis = 0;
            }

        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 4;

        if (isset($request->password) && $request->password != "") {
            $user->password = bcrypt($request->password);
        }
	
        $user->save();

        $student->STUDENT_NAME = $request->name;
        $student->FACULTY_ID = $request->faculty;
        $student->DEPARTMENT_ID = strcmp($request->department, 'select department') != 0 ? $request->department : -1;
        $student->class_id = strcmp($request->class, 'select class') != 0 ? $request->class : -1;

        $student->STUDENT_SSN = $request->ssn;
        $student->STUDENT_PASSWORD = bcrypt($request->password);
        $student->STUDENT_EMAIL = $request->email;
        $student->stage = $request->stage;
        // $student->term = $request->term;
        // $student->semester = $request->year;
        // $student->phone = $request->phone;
        $student->user_id = $user->id;
        if ($newRegisteration) {
            $studentcode = UUID::generate(8, 'App\Student', 'student_code');
            $student->student_code = $studentcode;
        }
        $studentcode = $student->student_code;
        $student->save();
        $responsecode = 1;

        //dd($responsecode);
        if ($english == '1') {
            return view('student.register', compact('responsecode', 'studentcode', 'newRegisteration'));
        } else {
            return view('student.registerarabic', compact('responsecode', 'studentcode', 'newRegisteration'));

        }
    }
    public function getStudentInformation(Request $request)
    {
        if ($request->ssn == "") {
            return 'no';
        }

        $studentinformation = StudentEmail::where('student_ssn', $request->ssn)->get()->first();

        if ($studentinformation) {

            return '{"student_name":"' . $studentinformation->student_name . '","student_email":"' . $studentinformation->student_email . '","student_password":"' . $studentinformation->student_password . '"}';
        }

        return 'no';
    }
    public function getRegisterdStudentInformation(Request $request)
    {
        if ($request->ssn == "") {
            return 'no';
        }

        $student = Student::where('STUDENT_SSN', $request->ssn)->first();
        if ($student) {
            return json_encode($student);
            //return '{"student_name":"'.$studentinformation->student_name.'","student_email":"'.$studentinformation->student_email.'","student_password":"'.$studentinformation->student_password.'"}';
        }

        return 'no';
    }
    public function getStudentIndex(Request $request)
    {
        $faculites = Faculty::all();
        $students = Student::where('DEPARTMENT_ID', $request->department)->get();
//        dd($students);
        return view('admin.students.index', compact('faculites', 'students'));
    }
    public function importExcel(Request $request)
    {
        set_time_limit(0);

        $request->validate([
            'import_file' => 'required',
            'faculty' => 'required',
            'department' => 'required',
        ]);

        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();
        //        dd($data);
        try {

            if ($data->count()) {
                foreach ($data as $key => $value) {
                    //dd($value);
                    foreach ($value as $k => $v) {

                        $us = User::where('email', $v['email'])->get()->first();
                        if ($us) {

                        } else {
                            $user = new User();
                            $user->name = $v['name'];
                            $user->email = $v['email'];
                            $user->role = 4;
                            $user->password = bcrypt('123456');
                            $user->save();
                            $student = new Student();
                            $student->STUDENT_NAME = $v['name'];
                            $student->FACULTY_ID = $request->faculty;
                            $student->DEPARTMENT_ID = $request->department;
                            $student->STUDENT_SSN = $v['snn'];
                            $student->STUDENT_PASSWORD = bcrypt('123456');
                            $student->STUDENT_EMAIL = $v['email'];
                            $student->term = $v['term'];
                            $student->semester = $v['year'];
                            $student->phone = $v['phone'];
                            $student->user_id = $user->id;
                            $student->save();
                        }

                    }

                }

                //if(!empty($arr)){
                //   Item::insert($arr);
                //}
            }
        } catch (\Exception $e) {
            echo $e;
        }
        //dd($arr);
        return redirect()->route('students.index');
    }

   

    public function store(StudentRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            return back()->withErrors(['error' => 'Email Found Before']);
        }
      //        dd($request->all());
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 4;
        $user->password = bcrypt($request->password);
        $user->save();
        $student = new Student();
        $student->STUDENT_NAME = $request->name;
        $student->FACULTY_ID = $request->faculty;
        $student->DEPARTMENT_ID = $request->department;
        $student->STUDENT_SSN = $request->snn;
        $student->STUDENT_PASSWORD = bcrypt($request->password);
        $student->STUDENT_EMAIL = $request->email;
        $student->term = $request->term;
        $student->semester = $request->year;
        $student->phone = $request->phone;
        $student->user_id = $user->id;
        $student->save();
        return redirect()->route('students.index');
    } //end of store

    public function update(StudentRequest $request, Student $student)
    {
        $user = $student->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 4;
        $user->password = bcrypt($request->password);
        $user->save();
        $student->STUDENT_NAME = $request->name;
        $student->FACULTY_ID = $request->faculty;
        $student->DEPARTMENT_ID = $request->department;
        $student->STUDENT_SSN = $request->snn;
        $student->STUDENT_PASSWORD = bcrypt($request->password);
        $student->STUDENT_EMAIL = $request->email;
        $student->term = $request->term;
        $student->semester = $request->year;
        $student->phone = $request->phone;
        $student->user_id = $user->id;
        $student->save();
        return redirect()->route('students.index');

    } //end of update

    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('students.index');

    } //end of destroy

    public function getDepartment(Request $request)
    {
        $depart_select = '';
        if ($request->depart) {
            $depart_select = $request->depart;
        }

        $departments = Department::where('FACULTY_ID', $request->id)->orderBy('id', 'desc')->get();
        //$data = '<option value="-1"> select department</option>';
        $data = '';
        foreach ($departments as $department) {
            $data .= '<option ';
//            if($depart_select ==$department->id)
            //                $data.= ' selected';
            $data .= ' value="' . $department->id . '">' . $department->DEPARTMENT_NAME . '</option>';
        }
        return $data;
    }
    public function getClass(Request $request)
    {
        $class_select = '';
        if ($request->class) {
            $class_select = $request->class;
        }

        $classes = DepartmentClass::where('DEPARTEMENT_ID', $request->id)->get();
        //$data = '<option value="-1"> select class</option>';
        $data = '';
        foreach ($classes as $class) {
            $data .= '<option ';
//            if($depart_select ==$department->id)
            //                $data.= ' selected';
            $data .= ' value="' . $class->id . '">' . $class->CLASS_NAME . '</option>';
        }
        return $data;
    }

    public function getStudents(Request $request)
    {
        $students = Student::where('DEPARTMENT_ID', $request->id)->get();
        $data = '';
//        dd($students);
        foreach ($students as $student) {
            $data .= '<tr class="gradeX" >
            <td>' . $student->STUDENT_NAME .
            '</td><td>' . $student->faculty->FACULTY_NAME . '</td ><td >';
            if ($student->term == 1) {
                $data .= 'First';
            } else {
                $data .= 'Second';
            }

            $data .= '</td ><td class="actions" >' .
            '<a href="#" class="on-default " data-toggle="modal"
                                   data-target="#con-close-modal_' . $student->id . '">
                                    <i style="color: white;padding: 8px 10px;background-color: #10c469 !important;border: 1px solid #10c469 !important;" class="fa fa-pencil"></i></a>'
            . '
                                <a href="#" data-toggle="modal" data-target="#dialog_' . $student->id . '"
                                   class="on-default ">
                                    <i style="color: white; background-color: #ff5b5b !important; border: 1px solid #ff5b5b !important;padding: 8px 10px;" class="fa fa-trash-o">
                                    </i>
                                </a></td >';
//            @include('admin.students.edit');
            $data .= $this->editform($student);
            $route = route('students.destroy', $student->id);
            $id = $student->id;
            $message = "Do You Want To Delete This Student ?";
            $data .= $this->deleteform($route, $id, $message);
//            @include('layouts.delete');
            $data .= '</tr>';
        }
        return $data;
    }

    public function editform($student)
    {
        $faculites = Faculty::all();

        $data = '
        <div id="con-close-modal_' . $student->id . '" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Edit    Faculty </h4>
                    </div>
                    <form action="' . route('students.update', $student->id) . '" method="post" data-parsley-validate novalidate>

                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="userName"> Name*</label>
                                    <input type="text" value="' . $student->STUDENT_NAME . '" name="name" parsley-trigger="change" required placeholder="Enter  name" class="form-control" id="userName">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="emailAddress">Email address*</label>
                                    <input type="email" name="email" value="' . $student->STUDENT_EMAIL . '" parsley-trigger="change" required placeholder="Enter email" class="form-control" id="emailAddress">
                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-lg-6">
                                    <label for="pass1">Password*</label>
                                    <input id="pass1" name="password" parsley-trigger="change" type="password" placeholder="Password"  required class="form-control">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label for="sel1">Select Colleges *</label>
                                    <select  name="faculty" class="form-control select2" onchange="department_ajax(this.value)">
                                        <option value="">Select Faculty</option>';
        foreach ($faculites as $facu) {
            $data .= '<option ' . $student->FACULTY_ID == $facu->id ? "selected" : '' . 'value="' . $facu->id . '"> ' . $facu->FACULTY_NAME . '</option>';
        }
        $data .= '</select>
                                </div>
                            </div>
                            <div class="row">
                              ';
        $departments_select = \App\Department::where('FACULTY_ID', $student->FACULTY_ID)->get();

        $data .= '<div class="form-group col-lg-6">
            <label for="sel1">Select Departement *</label>
            <select name="department" class="form-control select2" id="depart">';
        foreach ($departments_select as $department) {
            $data .= '<option' . $student->DEPARTMENT_ID == $department->id ? "selected" : "" . 'value="' . $department->id . '"> ' . $department->DEPARTMENT_NAME . '</option>';

        }

        $data .= '</select>
        </div>
        <div class="form-group col-lg-6">
            <label for="sel1">Select Term *</label>
            <select name="term" class="form-control select2">';
        $data .= '<option ' . $student->term == 1 ? 'selected' : ' ' . ' value="1"> First</option>';
        $data .= '<option ' . $student->term == 2 ? 'selected' : ' ' . ' value="2">Second</option>';
        $data .= '</select>
        </div>
        </div>
        <div class="row">

            <div class="form-group col-lg-6">
                <label for="sel1">Select Year *</label>
                <select name="year" class="form-control select2">';
        $data .= '<option ' . $student->semester == '2020 -2021' ? "selected" : '' . ' value="2020 -2021"> 2020 -2021</option>';
        $data .= '<option ' . $student->semester == '2019 -2020' ? "selected" : '' . ' value="2019 -2020"> 2019 -2020</option>';
        $data .= '<option ' . $student->semester == '2018 -2019' ? "selected" : '' . ' value="2018 -2019"> 2018 -2019</option>';
        $data .= '<option ' . $student->semester == '2017 -2018' ? "selected" : '' . ' value="2017 -2018"> 2017 -2018</option>';

        $data .= '
                </select>
            </div>
            <div class="form-group col-lg-6 ">
                <label>Code</label>
                <input type="text" name="phone" placeholder="" value="' . $student->phone . '" class="form-control" data-parsley-id="10">
                <span class="font-13 text-muted"></span>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-lg-6">
                <label for="userName"> SNN*</label>
                <input type="text" name="snn" parsley-trigger="change" value=""' . $student->STUDENT_SSN . '" required placeholder="Enter  SNN" class="form-control" id="userName">
            </div>
        </div>

        </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-info waves-effect waves-light">Edit </button>
        </div>
        </form>
        </div>
        </div>
        </div>';
        return $data;

    }

    public function deleteform($route, $id, $message)
    {
        $data = '
        <div id="dialog_' . $id . '" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">';
        $data .= '<div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
    Warming
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><span style="color: red;"></span></h4>
            </div>
            <div class="modal-body">' .
            $message
            . '</div>
            <div style="clear: both"></div>
            <div class="modal-footer">
                <form action=' . $route . '" method="post" style="display: inline;">';
        echo csrf_field();
        echo method_field('delete');
        $data .= '<button type="submit" class="btn btn-primary">Yes</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>

            </div>
        </div>

    </div>
</div><!-- /.modal -->';
        return $data;
    }

}
