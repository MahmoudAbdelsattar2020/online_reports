<?php

namespace App\Exports;

use App\Stud_ques_ans_choice;
use App\StudentExam;
use Maatwebsite\Excel\Concerns\FromArray;

class Result implements FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $grades;

    public function __construct(array $grades)
    {
        $this->grades = $grades;
    }

    public function array(): array
    {
        return $this->grades;
    }
}
