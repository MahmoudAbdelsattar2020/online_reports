<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentEmail extends Model
{
    protected $table = "student_emails";
    public $timestamps = false;
}
