
<div class="editable-responsive">
<hr/>
	<span style="padding:10px;"> Total Reports:<?php echo $student_cource_exam->count(); ?></span>
	<div style="float:right;">
		<a href="{{route('doctorProfile.exam.downloadallreports', $exam_id)}}"> <i class="fa fa-file-zip-o"></i> Download All</a> &nbsp;|&nbsp;
		<a href="{{route('doctorProfile.exam.result.downloadResult',$exam_id)}}"><i class="fa fa-file-excel-o"></i> 
		Download Grades Sheet</a>
	</div>
<hr/>
 <table class="table table-striped" id="datatable-editable">
	<thead>
	<tr>
       <th>Student</th>
       <th>Course</th>
       <th>Grade</th>
       <th>Operation</th>
   </tr>
   </thead>
   <tbody>
@foreach ($student_cource_exam as $k=>$s)

      <tr>
          <td>{{ $s->student->STUDENT_NAME }}</td>
          <td>{{$s->exam->course->COURSE_NAME}}</td>
          <td>
          <input type="text" id="{{'grade'.$k}}" style="margin:0px;width:70px;text-align:center;" value="{{$s->result}}" disabled required/>
          <input type="text" id="{{'copygrade'.$k}}" style="margin:0px;width:70px;text-align:center;" value="{{$s->result}}" hidden/>
          <button style="margin:0px;" id="{{'editbutton'.$k}}" class="btn btn-primary waves-effect waves-light" onclick="editButtonClick({{$k}})">Edit</button>
          <button style="margin:0px;display:none;background-color:green;color:white;" id="{{'savebutton'.$k}}" class="btn waves-effect waves-light" onclick="saveGrade({{$s->exam_id}},{{$s->student->id}},{{$k}})" >Save</button>
          <button style="margin:0px;display:none;background-color:#282C34;color:white;" id="{{'cancelbutton'.$k}}" class="btn waves-effect waves-light" onclick="cancelButtonClick({{$k}})">Cancel</button>
          </td>
          <td>
            <a href="{{route('doctorProfile.exam.downloadstudentreport', [$s->exam_id, $s->student->id, $s->exam->course->id]) }}" target="_blank"><span class="fa fa-download"></span>Download</a>
            <a href=""onclick="getPreviewBodyContent({{$s->exam_id}},{{$s->student->id}});" data-toggle="modal" data-target="#con-close-modal"><span class="fa fa-eye"></span>View</a>

          </td>
      </tr>
@endforeach
</tbody>
	<tfoot>
		<tr>
			<td colspan="4"><span>Total Reports:<?php echo $student_cource_exam->count(); ?></span></td>
		</tr>
	</tfoot>
</table>
<div id="con-close-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">File Preview </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div id="previewbody" class="container" style="width:100%;">
                  </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>

        </div>
    </div>
</div>
</div>
<script>
function getPreviewBodyContent(exam_id,student_id)
{
  $.ajax({ //Process the form using $.ajax()
      type: 'POST', //Method type
      url: '{{route('doctorProfile.exam.getpreviewbodycontent')}}', //Your form processing file URL
      data: {examid:exam_id,studentid:student_id, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
      success: function (data) {
          //alert(data);
          $('#previewbody').html(data);
      }
  });
}
function editButtonClick(idindex){
  //alert('dadadada');
  $('#editbutton'+idindex).hide();
  $('#savebutton'+idindex).show();
  $('#cancelbutton'+idindex).show();
  $('#grade'+idindex).prop('disabled',false);
}

function cancelButtonClick(idindex){
  $('#editbutton'+idindex).show();
  $('#savebutton'+idindex).hide();
  $('#cancelbutton'+idindex).hide();
  $('#grade'+idindex).prop('disabled',true);
  $('#grade'+idindex).val($('#copygrade'+idindex).val());
}

function saveGrade(exam_id,student_id,idindex)
{

  var value=$('#grade'+idindex).val();
  //alert(student_id);
  //alert(exam_id);

  if(isNaN(value))
  {
    alert('Invalid numeric value');
    $('#grade'+idindex).val($('#copygrade'+idindex).val());
  }
  else
  {
    //alert('dadadd');
    $.ajax({ //Process the form using $.ajax()
        type: 'POST', //Method type
        url: '{{route('doctorProfile.exam.evaluatereport')}}', //Your form processing file URL
        data: {examid:exam_id,studentid:student_id,grade: value, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
        success: function (data) {
            //alert(data);
            if(data==='yes')
            {
              $('#editbutton'+idindex).show();
              $('#savebutton'+idindex).hide();
              $('#cancelbutton'+idindex).hide();
              $('#grade'+idindex).prop('disabled',true);
              $('#copygrade'+idindex).val($('#grade'+idindex).val());
            }
            else
            {
              alert('Please, try again.');
            }
            //$('#table').html(data);
        }
    });
    //alert('dadadds');
  }

}
</script>
