<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title> online  Student-Reports System</title>

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

</head>

<body>

  <div class="container" style="background-color:white;">
    <div class="row text-center">
        <div class="col-md-8 col-md-offset-2">
          <div class="col-md-2">
            <img class="img-responsive logo-img" src="{{asset('assets/images/ic.jpg')}}">
          </div>
          <div class="col-md-8">
          <a href="{{ route('login')}}" class="logo"><span> <span>منصة </span> جامعة أسيوط للتسجيل <span>الالكترونى</span></span><i class="zmdi zmdi-layers"></i></a>
          </div>
          <div class="col-md-2">
            <img class="img-responsive logo-img" src="{{asset('assets/images/logo.png')}}">
          </div>

        </div>
    </div>
      <br/>
      <hr/>
      <br/>
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">

                  <div class="panel-heading">البريد الاكاديمى للطلاب</div>

                  <div class="panel-body">
                      <form class="form-horizontal" onsubmit="return validateRegisterationForm()">
                          {{ csrf_field() }}

                          <div class="form-group{{ $errors->has('ssn') ? ' has-error' : '' }}">
                              <label for="name" class="col-md-4 control-label">الرقم القومى</label>

                              <div class="col-md-6">
                                  <input id="ssn" type="text" class="form-control" name="ssn" pattern="[0-9]{14}" title="14 digits SSN number" onchange="studentInformation_ajax(this.value);"required autofocus>

                                  @if ($errors->has('ssn'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('ssn') }}</strong>
                                      </span>
                                  @endif
                              </div>
                              <div class="col-md-2" style="padding-top:10px;">قم بالضغط على Enter</div>
                              <div class="col-md-8 col-md-offset-4" id="ssn-status">
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                              <label for="name" class="col-md-4 control-label">الاسم</label>

                              <div class="col-md-6">
                                  <input id="name" type="text" class="form-control" name="name">

                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>


                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email" class="col-md-4 control-label">البريد الاكاديمى</label>

                              <div class="col-md-6">
                                  <input id="email" type="email" class="form-control" name="email">

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                              <div class="col-md-8 col-md-offset-4" id="email-status">
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="password" class="col-md-4 control-label">كلمة المرور</label>

                              <div class="col-md-6">
                                  <input id="password" type="text" class="form-control" name="password">

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          <div id="error-status" class="col-md-8 col-md-offset-4">

                          </div>
                          <div class="form-group">
                              <div class="col-md-6 col-md-offset-6">
                                <button type="button" class="btn btn-primary" onclick="resetForm();">
                                  مسح
                                </button>
                                  <button type="button" class="btn btn-primary" onclick="saveStudentMail();">
                                      حفظ
                                  </button>
                                  <button type="button" class="btn btn-danger" onclick="deleteStudentMail();" id="deletebtn"disabled>
                                      حذف
                                  </button>
                              </div>
                          </div>
                      </form>
                  </div>
                 <br/>
                  <br/>

              </div>
          </div>
      </div>

  </div>



<script>
  function resetForm()
  {
    $("#ssn").val('');
    $("#email").val('');
    $("#name").val('');
    $("#password").val('');
    $("#error-status").html('');
    $("#deletebtn").attr('disabled',true);
  }
  function deleteStudentMail()
  {

    var res=confirm('هل تريد الحذف؟');
    if(res==true)
    {
      $.ajax({
        type: 'POST', //Method type
        url: '{{route('deletestudentmail')}}', //Your form processing file URL
        data: {ssn: $("#ssn").val(), _token: "{{csrf_token()}}"}, //Forms name
        //dataType  : 'json',
        success: function (data) {
          if(data=="yes")
          {
            $("#deletebtn").attr('disabled',true);
            $("#error-status").html('<span style="color:green;">تم الحذف بنجاح</span>');
          }
          else {
            $("#error-status").html('<span style="color:red;">لم يتم الحذف</span>');

          }
        }
      });
    }
  }
  function deleteStudentMailByMail()
  {

    var res=confirm('هل تريد الحذف؟');
    if(res==true)
    {
      $.ajax({
        type: 'POST', //Method type
        url: '{{route('deletestudentmailbymail')}}', //Your form processing file URL
        data: {email: $('#email').val(), _token: "{{csrf_token()}}"}, //Forms name
        //dataType  : 'json',
        success: function (data) {
			
          if(data=="yes")
          {
            $("#deletebtn").attr('disabled',true);
            $("#error-status").html('<span style="color:green;">تم الحذف بنجاح</span>');
			$('#email-status').html('');
			//resetForm();
          }
          else {
            $("#error-status").html('<span style="color:red;">لم يتم الحذف</span>');

          }
        }
      });
    }
  }
  function saveStudentMail()
  {
    if($("#ssn").val()=='')
    {
      $("#error-status").html('<span style="color:red;">لا بد من ادخال الرقم القومى</span>');
      return;
    }
    if($("#name").val()=='')
    {
      $("#error-status").html('<span style="color:red;">لا بد من ادخال الاسم</span>');
      return;
    }
    if($("#email").val()=='')
    {
      $("#error-status").html('<span style="color:red;">لا بد من ادخال البريد الالكترونى</span>');
      return;
    }
    if($("#password").val()=='')
    {
      $("#error-status").html('<span style="color:red;">لا بد من ادخال كلمة المرور</span>');
      return;
    }

    $.ajax({ //Process the form using $.ajax()
        type: 'POST', //Method type
        url: '{{route('storestudentmail')}}', //Your form processing file URL
        data: {ssn: $("#ssn").val(),name:$("#name").val(),email:$("#email").val(),password:$("#password").val(), _token: "{{csrf_token()}}"}, //Forms name
        //dataType  : 'json',
        success: function (data) {
          //alert(data);
          if(data=="yes")
          {
              $("#error-status").html('<span style="color:green;">تم الحفظ بصورة صحيحة</span>');
          }
          else {
              $("#error-status").html('<span style="color:red;">'+data+'</span>');
          }
        }
      });

  }
    var resizefunc = [];
    function validateRegisterationForm()
    {
      var pass=document.getElementById("password").value;
      var passc=document.getElementById("password-confirm").value;
      //alert('test');
      if(pass.localeCompare(passc)!=0)
      {
        document.getElementById("password-confirm-status").innerHTML='<span style="color:red;">Does not match the password</span>';
        //alert('2');
        return false;
      }
      //alert('3');


      var fac = document.getElementById("faculty");
      var facultyvalue = fac.options[fac.selectedIndex].value;
      //alert(facultyvalue);
      if(facultyvalue==-1)
      {
        //alert('4');
        document.getElementById("faculty-status").innerHTML='<span style="color:red;">Please, select the faculty</span>';

        return false;
      }

      return true;
    }

    function studentInformation_ajax(val) {
      //alert('dadd');
      if(val=="")
      {
        $("#name").val('');
        $("#email").val('');
        $("#password").val('');
        $("#ssn-status").html('');
        $("#deletebtn").attr('disabled',true);
        return;
      }
      var ptt=/(([0-9]{14})|((aun)[a-zA-Z]{0,1}[0-9]+))/;
      var v=$("#ssn").val();
      //alert(v);
      if(!ptt.test(v))
      {
        $("#ssn").val('');
        alert('الرقم القومى غير صالح');
        return;
      }
      //  alert('1');
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getStudentInformation')}}', //Your form processing file URL
            data: {ssn: val, _token: "{{csrf_token()}}"}, //Forms name
            //dataType  : 'json',
            success: function (data) {
      //alert(data);
            //alert(data);
            if(data=='no')
            {
              $("#name").val('');
              $("#email").val('');
              $("#password").val('');
                $("#deletebtn").attr('disabled',true);
            }
            else {
              //  alert(JSON.parse(data));
                var studentInformation=JSON.parse(data);
                // alert('ddad');
                // alert(studentInformation);
                // alert(studentInformation.student_name);
                // alert(studentInformation.student_email);

                $("#name").val(studentInformation.student_name);
                $("#email").val(studentInformation.student_email);
                $("#password").val(studentInformation.student_password);
                $("#ssn-status").html('');
                  $("#deletebtn").attr('disabled',false);

            }
                //$('#depart').select2('destroy')

                //$('#depart').select2()


            }
        });
    }
    function department_ajax(val) {
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getDepartment')}}', //Your form processing file URL
            data: {id: val, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
            success: function (data) {
      //alert(data)

                //$('#depart').select2('destroy')
                $('#depart').html(data)
                //$('#depart').select2()


            }
        });
    }
    function class_ajax(val) {

        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getClass')}}', //Your form processing file URL
            data: {id: val, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
            success: function (data) {
      //alert(data)
            //alert(data)
                //$('#depart').select2('destroy')
                $('#class').html(data)
                //$('#depart').select2()


            }
        });
    }
    function existmail_ajax(val) {
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('existEmail')}}', //Your form processing file URL
            data: {email: val, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
            success: function (data) {
      //alert(data)
                //alert(data);
                //$('#depart').select2('destroy')
                if(data.localeCompare('yes')==0)
                {
                  $('#email-status').html('<span style="color:red">البريد الالكترونى موجود بالفعل</span><a href="#" class="btn" onClick="deleteStudentMailByMail();">حذف </a>')
                }
				else
				{
					$('#email-status').html('');
				}
                //$('#depart').html(data)
                //$('#depart').select2()


            }
        });
    }
</script>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

</body>

</html>
