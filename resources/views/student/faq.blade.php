<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title> online  Student-Reports System</title>

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

</head>

<body>

  <div class="container" style="background-color:white;">
    <div class="row text-center">
        <div class="col-md-8 col-md-offset-2">
          <div class="col-md-2">
            <img class="img-responsive logo-img" src="{{asset('assets/images/ic.jpg')}}">
          </div>
          <div class="col-md-8">
          <a href="{{ route('login')}}" class="logo"><span> <span>منصة </span> جامعة أسيوط للتسجيل <span>الالكترونى</span></span><i class="zmdi zmdi-layers"></i></a>
          </div>
          <div class="col-md-2">
            <img class="img-responsive logo-img" src="{{asset('assets/images/logo.png')}}">
          </div>

        </div>
    </div>
      <br/>
      <hr/>
      <br/>
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">

                  <div class="panel-heading">الاسئلة الشائعة والملاحظات</div>

                  <div class="panel-body">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">متى يبدا رفع الابحاث؟</h4>
                        <p class="card-text">يبدا رفع الابحاث من يوم 30/5 وحسب الجدول المعلن لكل كلية</p>
                        <br/>
                      </div>
                      <hr/>
                      <div class="card-body">
                        <h4 class="card-title">الرقم القومي الخاص بي غير مسجل؟</h4>
                        <p class="card-text">الاجابه الرجاء تسجيل بياناتك في صفحه التسجيل والتواصل مع شؤون الطلاب لتحديث بياناتك</p>
                        <br/>

                      </div>
                      <hr/>
                      <div class="card-body">
                        <h4 class="card-title">كيف اقوم بتفعيل البريد الالكتروني الخاص بي ؟</h4>
                        <p class="card-text">راجع الفيديو المرفق</p>
                        <br/>
                        <a class="btn btn-primary" href="https://youtu.be/5zg9WHybRTI" class="card-link">الفيديو</a>

                      </div>
                      <div class="card-body">
                        <h4 class="card-title">الباسورد بتاعتي بتطلع فاضيه؟</h4>
                        <p class="card-text">يمكنك تسجيل شكوى من خلال صفحة الشكاوى</p>
                        <br/>
                        <a class="btn btn-primary" href="{{route('complains')}}" class="card-link">صفحة الشكاوى</a>

                      </div>
                      <div class="card-body">
                        <h4 class="card-title">هستخدم الكود الذى ظهر في نهاية التسجيل في ايه؟</h4>
                        <p class="card-text">سوف يتم استخدام هذا الكود عند رفع الابحاث الخاصة بك</p>
                        <br/>
                      </div>
                      <div class="card-body">
                        <h4 class="card-title">هل كلمة المرور في صفحة التسجيل هي كلمة المرور الخاصة بالبريد الاكاديمي؟</h4>
                        <p class="card-text">لا يمكن ان تدخل اي كلمة مرور لكن احتفظ بها لانك سوف تستخدمها في الدخول لرفع الابحاث</p>
                        <br/>

                      </div>
                      <div class="card-body">
                        <h4 class="card-title">متى يتم الرد على الشكوى الخاصه بي؟</h4>
                        <p class="card-text">يتم الرد على الشكوى الخاصه بك خلال 48 ساعه</p>
                        <br/>

                      </div>
                      <div class="card-body">
                        <h4 class="card-title">الرقم القومي الخاص بي غير مسجل ؟</h4>
                        <p class="card-text">الرجاء تسجيل بياناتك في صفحه التسجيل والتواصل مع شؤون الطلاب لتحديث بياناتك</p>
                        <br/>

                      </div>
                      <div class="card-body">
                        <h4 class="card-title">متى يتم الرد على الشكوى الخاصه بي؟</h4>
                        <p class="card-text">يتم الرد على الشكوى الخاصه بك خلال 48 ساعه</p>
                        <br/>

                      </div>

                    </div>
                  </div>
                 <br/>
                  <br/>

              </div>
          </div>
      </div>

  </div>



<script>
    var resizefunc = [];
    function validateRegisterationForm()
    {
      var pass=document.getElementById("password").value;
      var passc=document.getElementById("password-confirm").value;
      //alert('test');
      if(pass.localeCompare(passc)!=0)
      {
        document.getElementById("password-confirm-status").innerHTML='<span style="color:red;">Does not match the password</span>';
        //alert('2');
        return false;
      }
      //alert('3');


      var fac = document.getElementById("faculty");
      var facultyvalue = fac.options[fac.selectedIndex].value;
      //alert(facultyvalue);
      if(facultyvalue==-1)
      {
        //alert('4');
        document.getElementById("faculty-status").innerHTML='<span style="color:red;">Please, select the faculty</span>';

        return false;
      }

      return true;
    }

    function studentInformation_ajax(val) {
      //alert('dadd');
      if(val=="")
      {
        $("#name").val('');
        $("#email").val('');
        $("#password").val('');
        $("#ssn-status").html('');

      }
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getStudentInformation')}}', //Your form processing file URL
            data: {ssn: val, _token: "{{csrf_token()}}"}, //Forms name
            //dataType  : 'json',
            success: function (data) {
      //alert(data);
            //alert(data);
            if(data=='no')
            {
              $("#name").val('');
              $("#email").val('');
              $("#password").val('');

              $("#ssn-status").html('<span style="color:red">الرقم القومى هذا غير مسجل الرجاء الرجوع لموظفى شئون الطلاب بكليتك</span>');
            }
            else {
              //  alert(JSON.parse(data));
                var studentInformation=JSON.parse(data);
                // alert('ddad');
                // alert(studentInformation);
                // alert(studentInformation.student_name);
                // alert(studentInformation.student_email);

                $("#name").val(studentInformation.student_name);
                $("#email").val(studentInformation.student_email);
                $("#password").val(studentInformation.student_password);
                $("#ssn-status").html('');

            }
                //$('#depart').select2('destroy')

                //$('#depart').select2()


            }
        });
    }
    function department_ajax(val) {
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getDepartment')}}', //Your form processing file URL
            data: {id: val, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
            success: function (data) {
      //alert(data)

                //$('#depart').select2('destroy')
                $('#depart').html(data)
                //$('#depart').select2()


            }
        });
    }
    function class_ajax(val) {

        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getClass')}}', //Your form processing file URL
            data: {id: val, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
            success: function (data) {
      //alert(data)
            //alert(data)
                //$('#depart').select2('destroy')
                $('#class').html(data)
                //$('#depart').select2()


            }
        });
    }
    function existmail_ajax(val) {
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('existEmail')}}', //Your form processing file URL
            data: {email: val, _token: "{{csrf_token()}}"}, //Forms name
//              dataType  : 'json',
            success: function (data) {
      //alert(data)
                //alert(data);
                //$('#depart').select2('destroy')
                if(data.localeCompare('yes')==0)
                {
                  $('#email-status').html('<span style="color:red">Exist email. Please, select another one.</span>')
                }
				else
				{
					$('#email-status').html('');
				}
                //$('#depart').html(data)
                //$('#depart').select2()


            }
        });
    }
</script>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

</body>

</html>
