<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title> online  Student-Reports System</title>

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

<style>

</style>
</head>

<body>
  <!-- style="background-image: url({{asset('assets/images/registeration-background.jpg')}});background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;" -->
  <div class="container" >
    <div class="row">
      <div class="col-md-12 text-center" style="background-color:#163D78;">
        <div class="col-md-8 col-md-offset-2">
        <a href="#" style="color:white;font-size:20pt;"><span> <span>منصة </span> جامعة أسيوط للتسجيل <span>الالكترونى</span></span></a><i class="zmdi zmdi-layers"></i>

        <br/>
        <br/>

          <h2 style="color:white;font-size:18pt;">إجمالى اعداد الطلاب </h2>

          <!-- <label for="faculty" style="color:white;" class="col-md-12 control-label">إختر الكلية</label>
          <select  id="faculty" name="faculty" class="form-control select2" onchange="counter_ajax(this.value)">
              <option value="0">الكل</option>
              @foreach($faculites as $facu)
              <option value="{{$facu->id}}" @if(old('faculty') ==$facu->id ) selected @endif > {{$facu->FACULTY_NAME}}</option>
              @endforeach
          </select> -->

          <label for="faculty" class="col-md-12 control-label" >
            <span id="countspan" style="color:white;font-size:18pt;"></span>
          </label>
          <br/>
          <div id="tablediv" class="col-md-12 table-responsive" style="background-color:white;">

          </div>
          &nbsp;

          </div>
      </div>

    </div>


</div>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

</body>
<script>
counter_ajax(0);
function counter_ajax(val) {

  $.ajax({ //Process the form using $.ajax()
      type: 'POST', //Method type
      url: '{{route('getstudentsregisterationcount')}}', //Your form processing file URL
      data: {id: val, _token: "{{csrf_token()}}"}, //Forms name
      //dataType  : 'json',
      success: function (data) {

        $("#countspan").html(data);
        $.ajax({ //Process the form using $.ajax()
            type: 'POST', //Method type
            url: '{{route('getstudentsregisterationcounttable')}}', //Your form processing file URL
            data: {id: val, _token: "{{csrf_token()}}"}, //Forms name
            //dataType  : 'json',
            success: function (data2) {
              //alert(data2);
              $("#tablediv").html(data2);
              //alert('daddas');
            }});
        }
      });
  }
</script>
</html>
