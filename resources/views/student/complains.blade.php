<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title> online  Student-Reports System</title>

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

</head>

<body>

  <div class="container" style="background-color:white;">
      <div class="row text-center">
          <div class="col-md-8 col-md-offset-2">
            <div class="col-md-2">
              <img class="img-responsive logo-img" src="{{asset('assets/images/ic.jpg')}}">
            </div>
            <div class="col-md-8">
            <a href="{{ route('login')}}" class="logo"><span> <span>منصة </span> جامعة أسيوط للتسجيل <span>الالكترونى</span></span><i class="zmdi zmdi-layers"></i></a>
            </div>
            <div class="col-md-2">
              <img class="img-responsive logo-img" src="{{asset('assets/images/logo.png')}}">
            </div>

          </div>
        </div>
        <br/>
  <div class="col-md-8 col-md-offset-2">
  <div class="row">
    <ul class="nav nav-tabs" style="float:right;">
        <li class="active"><a data-toggle="tab" href="#complain">تسجيل شكوى</a></li>
        <li><a data-toggle="tab" href="#enquery">الاستعلام عن شكوى</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade in active" id="complain">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">إستمارة تسجيل شكوى</div>

                    <div class="panel-body">
                        <form id="registerform" class="form-horizontal" method="POST" action="{{ route('storestudent',['0','1']) }}" onsubmit="return validateRegisterationForm()">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('ssn') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">الرقم القومى<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <input id="ssn" type="text" class="form-control" name="ssn" value="{{ old('ssn') }}" pattern="[0-9]{14}" title="14 digits SSN number" required autofocus>

                                    @if ($errors->has('ssn'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ssn') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-8 col-md-offset-4" id="ssn-status">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">الاسم<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">البريد الالكترونى الاكاديمى<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-8" id="email-status">
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="mobilenumber" class="col-md-4 control-label">رقم الموبايل<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <input id="mobilenumber" type="mobilenumber" class="form-control" name="mobilenumber" value="{{ old('mobilenumber') }}"  required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-8" id="email-status">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="sel1" class="col-md-4 control-label">الكلية <span style="color:red;">*</span></label>
                                <div class="col-md-6">
                                  <select  id="faculty" name="faculty" class="form-control select2" onchange="department_ajax(this.value)">
                                    <option value="-1">إختر الكلية</option>

                                </select>
                                <div class="col-md-8" id="faculty-status">
                                </div>
                              </div>
                            </div>
                            <div class="form-group{{ $errors->has('stage') ? ' has-error' : '' }}">
                                <label for="stage" class="col-md-4 control-label">المستوى<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <select  id="stage" name="stage" class="form-control select2">
                                    <option value="0" selected>إعدادى</option>
                                      <option value="1" @if(old('stage') == 1) selected @endif>المستوى الاول</option>
                                      <option value="2" @if(old('stage') == 2) selected @endif>المستوى الثانى</option>
                                      <option value="3" @if(old('stage') == 3) selected @endif>المستوى الثالث</option>
                                      <option value="4" @if(old('stage') == 4) selected @endif>المستوى الرابع</option>
                                      <option value="5" @if(old('stage') == 5) selected @endif>المستوى الخامس</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('stage') ? ' has-error' : '' }}">
                                <label for="stage" class="col-md-4 control-label">نوع الشكوى<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <select  id="complaintype" name="complaintype" class="form-control select2">
                                    <option value="0" selected>إختر نوع الشكوى</option>
                                      <option value="1" @if(old('complaintype') == 1) selected @endif>شكوى 1</option>
                                      <option value="2" @if(old('complaintype') == 2) selected @endif>شكوى 1</option>
                                      <option value="3" @if(old('complaintype') == 3) selected @endif>شكوى 1</option>
                                      <option value="4" @if(old('complaintype') == 4) selected @endif>شكوى 1</option>
                                      <option value="5" @if(old('complaintype') == 5) selected @endif>شكوى 1</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="complaintext">نص الشكوى</label>
                              <div class="col-md-6">
                                <textarea name="complaintext" class="form-control" parsley-trigger="change"
                                    class="form-control" id="complaintext"></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-6">
                                  <button type="button" class="btn btn-primary">
                                    مسح
                                  </button>
                                  <button type="submit" class="btn btn-primary" id="submitformbutton">
                                      تسجيل
                                  </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      </div>
      <div class="tab-pane fade" id="enquery" >
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">إستعلام عن شكوى</div>

                    <div class="panel-body">
                        <form id="registerform" class="form-horizontal" method="POST" action="{{ route('storestudent',['0','1']) }}" onsubmit="return validateRegisterationForm()">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('ssn') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">الرقم القومى<span style="color:red;">*</span></label>

                                <div class="col-md-6">
                                    <input id="enqueryssn" type="text" class="form-control" name="enqueryssn" value="{{ old('enqueryssn') }}" pattern="[0-9]{14}" title="14 digits SSN number" required autofocus>
                                </div>
                                <div class="col-md-3" >
                                  <button type="submit" class="btn btn-primary" >
                                      تسجيل
                                  </button>
                                </div>
                            </div>
                        </form>
                        <hr/>
                        <div class="table-responsive">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>رقم الشكوى</th>
                                <th>نص الشكوى</th>
                                <th>الرد على الشكوى</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>نصصصصصصصص يمينشيشنيم شسنيتشي شسنيتشيتشمي شمي</td>
                                  <td>نصصصصصصصص يمينشيشنيم شسنيتشي شسنيتشيتشمي شمي</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>نصصصصصصصص يمينشيشنيم شسنيتشي شسنيتشيتشمي شمي</td>
                                  <td>نصصصصصصصص يمينشيشنيم شسنيتشي شسنيتشيتشمي شمي</td>
                                </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


      </div>

    </div>
  </div>
  </div>

</div>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

</body>

</html>
