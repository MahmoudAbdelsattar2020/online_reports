@extends('layouts.app')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-body">


                <div class="editable-responsive ">

                    <!-- datatable-editable -->
                    <table class="table table-striped" id="faculty">
                        <thead>
                            <tr>
                                <th>Report Name</th>
                                <th>Course Name</th>
                                <th>Deadline (days)</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($exams as $k=>$exam)
                            @if($exam->type==4)
                            @if($exam->date <= $today_date && $today_date<=date("m/d/Y",strtotime($exam->date.'+'.($exam->EXAM_DURATION).' days')))
                            <tr class="gradeX">
                                <th>{{$exam->EXAM_NAME}}</th>
                                <th> {{$exam->COURSE_NAME}}</th>
                                <th>{{$exam->date}}</th>
                                <th class="actions">
                                        <form action="{{route('student.exam.start',$exam->id)}}" method="post">
                                            {!! csrf_field() !!}
                                            <input type="hidden" value="1" name="sadasdjkasj">
                                            <input type="submit" value="Start" class="btn btn-primary">
                                        </form>

                                        {{--<a href="{{route('student.exam.start',$exam->id)}}"--}}
                                        {{--class="btn btn-primary"> Start</a>--}}
                                </th>
                            </tr>
                            @endif
                            @else
                            @if($exam->date == $today_date && date("H:i") <=date( "H:i" ,
                            strtotime('+'.($exam->EXAM_DURATION) . ' minutes', strtotime($exam->time) ) ) )
                            @if( $exam->allow_time  <= date("H:i") )
                            <tr class="gradeX">
                                <th>{{$exam->EXAM_NAME}}</th>
                                <th> {{$exam->COURSE_NAME}}</th>
                                <th>{{$exam->date}}</th>
                                <th class="actions">
                                    @if( $exam->time <= date("H:i") && date("H:i") <=date( "H:i" ,
                                        strtotime('+'.($exam->EXAM_DURATION) . ' minutes', strtotime($exam->time) ) )
                                        )
                                        <form action="{{route('student.exam.start',$exam->id)}}" method="post">
                                            {!! csrf_field() !!}
                                            <input type="hidden" value="1" name="sadasdjkasj">
                                            <input type="submit" value="Start" class="btn btn-primary">
                                        </form>
                                        @endif
                                        {{--<a href="{{route('student.exam.start',$exam->id)}}"--}}
                                        {{--class="btn btn-primary"> Start</a>--}}
                                </th>
                            </tr>
                            @endif
                            @endif
                            @endif
                            @endforeach


                        </tbody>
                    </table>
					</hr/>
					<table class="table table-responsive table-striped">
                        <tr>
                            <th>Course Name</th>
							<th>Report Name</th>
                            <th>Status</th>
							<th></th>
							
                        </tr>
                        @foreach($data as $k=>$reportattachment)
                        <tr class="gradeX">
							<td>{{$reportattachment['course_name']}}</td>
                            <td>{{$reportattachment['exam_name']}}</td>
							@if(file_exists(public_path('/'.$reportattachment['path'])))
								<td><span style="color:green;"> تم التسليم </span></td>
								<td></td>
							@else
								<td><span style="color:red;">لم يتم تحميل التقرير </span></td>
								<td>
									<form action="{{route('student.reuploadreport',$reportattachment['id'])}}" id="uploadform{{$k}}" method="post" onsubmit="return validateUploadForm({{$k}});" enctype="multipart/form-data"   >
										{!!  csrf_field()!!}
									  <div class="row">
										<div class="col-md-12 question">

											<div>
													<div class="col-md-11" style="margin-bottom: 30px">
													  <div class="dropdown pull-right">
													  </div>
														<input type="file" id="filereport{{$k}}" name="filereport" class="dropify" data-max-file-size="10M" onchange="file_check(this,{{$k}});"
														accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf"/>
														<br/>
														<input type="hidden" name="reportid" value="{{$reportattachment['id']}}"/>
														<div class="col-md-11" id="filestatus{{$k}}">
														</div>
													</div>
													<br/>
													<br/>
													<br/>
													<div class="row">
													  <div class="hidden-print">
														  <div class="pull-right">
															  {{--<a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i--}}
															  {{--class="fa fa-print"></i></a>--}}
															  <button type="submit" id="submitbutton" style="background-color:#282C34;color:white;"class="btn waves-effect waves-light">Upload</button>
														  </div>
													  </div>
													</div>
											  </div>
											</div>
										  </div>

									  </form>
								</td>
						
							@endif
                            
                        </tr>
                        @endforeach
                    </table>
					<hr/>
                    <table class="table table-responsive table-striped">
                        <tr>
                            <th>Report Name</th>
                            <th>Result</th>
                        </tr>
                        @foreach($studentExams as $studentExam)
                        <tr class="gradeX">
                            <th>{{$studentExam->exam->EXAM_NAME}}</th>
                            <th> {{$studentExam->result}}</th>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <!-- end: panel body -->

        </div> <!-- end panel -->
    </div> <!-- end col-->
</div>

@endsection
@section('script')
<script>
	function file_check(elem,id)
	{
		var myfile= elem.val();
	   var ext = myfile.split('.').pop();
	   if(!(ext=="pdf" || ext=="docx" || ext=="doc")){
			elem.val("");
			var drEvent = $(this).dropify();
			drEvent = drEvent.data('dropify');
			drEvent.resetPreview();
			drEvent.clearElement();
			$('#filestatus'+id).html('<span style="color:red;">Invalid file type. Your file must be in word of PDF format.</span>');
			//$('#submitbutton').prop('disabled',false);
	   }
	   else {
		   $('#filestatus').html('');
	   }
	}
</script>
<script>
	
    (function ($) {

            'use strict';

            var EditableTable = {

                options: {
                    addButton: '#addToTable',
                    table: '#faculty',
                    dialog: {
                        wrapper: '#dialog',
                        cancelButton: '#dialogCancel',
                        confirmButton: '#dialogConfirm',
                    }
                },

                initialize: function () {
                    this
                        .setVars()
                        .build()
                        .events();
                },

                setVars: function () {
                    this.$table = $(this.options.table);
                    this.$addButton = $(this.options.addButton);

                    // dialog
                    this.dialog = {};
                    this.dialog.$wrapper = $(this.options.dialog.wrapper);
                    this.dialog.$cancel = $(this.options.dialog.cancelButton);
                    this.dialog.$confirm = $(this.options.dialog.confirmButton);

                    return this;
                },

                build: function () {
                    this.datatable = this.$table.DataTable({
                        aoColumns: [
                            null,
                            null,
                            null,
                            {"bSortable": false}
                        ]
                    });

                    window.dt = this.datatable;

                    return this;
                },

                events: function () {
                    var _self = this;

                    this.$table
                        .on('click', 'a.save-row', function (e) {
                            e.preventDefault();

                            _self.rowSave($(this).closest('tr'));
                        })
                        .on('click', 'a.cancel-row', function (e) {
                            e.preventDefault();

                            _self.rowCancel($(this).closest('tr'));
                        })
                        .on('click', 'a.edit-row', function (e) {
                            e.preventDefault();

                            _self.rowEdit($(this).closest('tr'));
                        })
                        .on('click', 'a.remove-row', function (e) {
                            e.preventDefault();

                            var $row = $(this).closest('tr');

                            $.magnificPopup.open({
                                items: {
                                    src: _self.options.dialog.wrapper,
                                    type: 'inline'
                                },
                                preloader: false,
                                modal: true,
                                callbacks: {
                                    change: function () {
                                        _self.dialog.$confirm.on('click', function (e) {
                                            e.preventDefault();

                                            _self.rowRemove($row);
                                            $.magnificPopup.close();
                                        });
                                    },
                                    close: function () {
                                        _self.dialog.$confirm.off('click');
                                    }
                                }
                            });
                        });

                    this.$addButton.on('click', function (e) {
                        e.preventDefault();

                        _self.rowAdd();
                    });

                    this.dialog.$cancel.on('click', function (e) {
                        e.preventDefault();
                        $.magnificPopup.close();
                    });

                    return this;
                },

                // ==========================================================================================
                // ROW FUNCTIONS
                // ==========================================================================================
                rowAdd: function () {
                    this.$addButton.attr({'disabled': 'disabled'});

                    var actions,
                        data,
                        $row;

                    actions = [
                        '<a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>',
                        '<a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>',
                        '<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
                        '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
                    ].join(' ');

                    data = this.datatable.row.add(['', '', '', actions]);
                    $row = this.datatable.row(data[0]).nodes().to$();

                    $row
                        .addClass('adding')
                        .find('td:last')
                        .addClass('actions');

                    this.rowEdit($row);

                    this.datatable.order([0, 'asc']).draw(); // always show fields
                },

                rowCancel: function ($row) {
                    var _self = this,
                        $actions,
                        i,
                        data;

                    if ($row.hasClass('adding')) {
                        this.rowRemove($row);
                    } else {

                        data = this.datatable.row($row.get(0)).data();
                        this.datatable.row($row.get(0)).data(data);

                        $actions = $row.find('td.actions');
                        if ($actions.get(0)) {
                            this.rowSetActionsDefault($row);
                        }

                        this.datatable.draw();
                    }
                },

                rowEdit: function ($row) {
                    var _self = this,
                        data;

                    data = this.datatable.row($row.get(0)).data();

                    $row.children('td').each(function (i) {
                        var $this = $(this);

                        if ($this.hasClass('actions')) {
                            _self.rowSetActionsEditing($row);
                        } else {
                            $this.html('<input type="text" class="form-control input-block" value="' + data[i] + '"/>');
                        }
                    });
                },

                rowSave: function ($row) {
                    var _self = this,
                        $actions,
                        values = [];

                    if ($row.hasClass('adding')) {
                        this.$addButton.removeAttr('disabled');
                        $row.removeClass('adding');
                    }

                    values = $row.find('td').map(function () {
                        var $this = $(this);

                        if ($this.hasClass('actions')) {
                            _self.rowSetActionsDefault($row);
                            return _self.datatable.cell(this).data();
                        } else {
                            return $.trim($this.find('input').val());
                        }
                    });

                    this.datatable.row($row.get(0)).data(values);

                    $actions = $row.find('td.actions');
                    if ($actions.get(0)) {
                        this.rowSetActionsDefault($row);
                    }

                    this.datatable.draw();
                },

                rowRemove: function ($row) {
                    if ($row.hasClass('adding')) {
                        this.$addButton.removeAttr('disabled');
                    }

                    this.datatable.row($row.get(0)).remove().draw();
                },

                rowSetActionsEditing: function ($row) {
                    $row.find('.on-editing').removeClass('hidden');
                    $row.find('.on-default').addClass('hidden');
                },

                rowSetActionsDefault: function ($row) {
                    $row.find('.on-editing').addClass('hidden');
                    $row.find('.on-default').removeClass('hidden');
                }

            };

            $(function () {
                EditableTable.initialize();
            });

        }).apply(this, [jQuery]);

        function courses_ajax(faculty_id, id) {
            $.ajax({ //Process the form using $.ajax()
                type: 'POST', //Method type
                url: '{{route('ilos.getCourses')}}', //Your form processing file URL
                data: {_token: "{{csrf_token()}}", faculty_id: faculty_id}, //Forms name
//              dataType  : 'json',
                success: function (data) {

//                    srt=''
                    if (id) {
                        $('#courses_' + id).select2('destroy');
                        $('#courses_' + id).html(data)
                        $('#courses_' + id).select2();

                    } else {
                        $('#courses').select2('destroy');
                        $('#courses').html(data)
                        $('#courses').select2();

                    }
                }
            });
        }
</script>
<script>
    history.pushState(null, null, null);
        window.addEventListener('popstate', function () {
            history.pushState(null, null,null);
        });
</script>
@endsection
