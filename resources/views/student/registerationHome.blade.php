<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title> online  Student-Reports System</title>

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

<style>
</style>
</head>

<body>
  <!-- style="background-image: url({{asset('assets/images/registeration-background.jpg')}});background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;" -->
  <div class="container" >
    <div class="row">


      <div class="col-md-8" style="background-color: white;">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <div class="row text-center">

        <div class="col-md-12">

          <div class="col-md-3">
          </div>
              <div class="col-md-3">
                <img class="img-responsive logo-img" src="{{asset('assets/images/ic.jpg')}}">
              </div>
              <div class="col-md-3">
                <img class="img-responsive logo-img" src="{{asset('assets/images/logo.png')}}">
              </div>
              <div class="col-md-3">
              </div>

          </div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>

            <div class="col-md-12">
            <a href="#" class="logo"><span> <span>منصة </span> جامعة أسيوط للتسجيل <span>الالكترونى</span></span></a><i class="zmdi zmdi-layers"></i>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>



      </div>
      <div class="col-md-4 text-center" style="background-color:#163D78;">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
          <a class="btn btn-primary" style="width:200px;font-size:14pt;background-color:#163D78 !important;" href="{{ route('enquerymail')}}">الاستعلام عن البريد الاكاديمى</a> <br/>
          <br/>
          <a class="btn btn-primary" style="width:200px;font-size:14pt;background-color:#163D78 !important;"href="{{ route('registerstudentarabic')}}">التسجيل</a><br/>
          <br/>
          <a class="btn btn-primary" style="width:200px;font-size:14pt;background-color:#163D78 !important;"href="{{route('login')}}">رفع الابحاث</a><br/>
          <br/>
          <a class="btn btn-primary" style="width:200px;font-size:14pt;background-color:#163D78 !important;" href="{{ route('faq')}}">اسئلة شائعة وملاحظات</a><br/>
          <br/>
          <a class="btn btn-primary" style="width:200px;font-size:14pt;background-color:#163D78 !important;" href="http://ec2-3-20-236-66.us-east-2.compute.amazonaws.com/complaintStudent/public/">الشكاوى</a><br/>
          <br/>
          <a class="btn btn-primary" style="width:200px;font-size:14pt;background-color:#163D78 !important;" href="https://youtu.be/Zc2jOR9SUkc">فيديو توضيحى</a><br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
      </div>

    </div>


</div>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

</body>

</html>
